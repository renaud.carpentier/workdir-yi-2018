# TP Realworld

Créer un nouveau paragraphe pour chaque section 6.x.y

## 6.3

### 6.3.1 
7a2e472d9d9e77ad7de8f4ace9e09d7597ee60f7
### 6.3.2
https://hub.docker.com/r/mkenney/npm/
.
Dockerfile for node-8-debian https://github.com/mkenney/docker-npm/blob/master/node-8-debian/Dockerfile
.
Volume : /src
.
docker image pull mkenney/npm:node-8-debian
.
9af19a7f4e2c
.
### 6.3.3
docker container run -ti --rm -v $(pwd):/src 9af19a7f4e2c npm install
.
### 6.3.4
docker container run -ti --rm -v $(pwd):/src -p 8000:8080 9af19a7f4e2c npm run dev
.
## 6.4 
.
### 6.4.1
eef3d052fe83dc688c0eb3dab97876d714a2d14d
.
### 6.4.2
f438b7d58d0a
.
Volume : /home/gradle/.gradle
.
### 6.4.3
docker volume create --name gradle-home
.
docker volume ls
.
local               c66fc03b1cc7be5447a458e33b5394b52255e551f6952f5e03de2c9833f4196d
local               eacb15c2749d6aa23ff76cb4d8997427a69c33294f72dd587f2c8ca43ee5cfaa
local               gradle-home
local               nginx-logs
.
### 6.4.4
docker container run -ti --rm -v gradle-home:/home/gradle/.gradle -v $(pwd):/src -w /src f438b7d58d0a gradle build
.
### 6.4.5
docker container run -ti --rm -v gradle-home:/home/gradle/.gradle -v $(pwd):/src -w /src -p 8001:8080 f438b7d58d0a gradle bootRun
.
{
articles: [ ],
articlesCount: 0
}
.
### 6.5.1
{"articles":[{"id":"e55e0030-8e7d-46c1-90a5-2a4d68c6c086","slug":"renowx","title":"renowx","description":"renowx renowx","body":"dfdqsdfqsdf","favorited":false,"favoritesCount":0,"createdAt":"2018-05-16T12:31:34.036Z","updatedAt":"2018-05-16T12:31:34.036Z","tagList":[],"author":{"username":"renowx","bio":"","image":"https://static.productionready.io/images/smiley-cyrus.jpg","following":false}}],"articlesCount":1}
.
### 6.6.1
docker container run -ti --rm -v $(pwd):/src mkenney/npm:node-8-debian npm run build
.
favicons-c2a605fbc0e687b2e1b4b90a7c445cdd	index.html					static
.
### 6.6.2
docker container run -ti --rm -v $(pwd)/dist:/usr/share/nginx/html -p 8000:80 nginx:alpine
.
### 6.6.3
docker image build -t renowx/realworldfront:vuejs --build-arg SHA1=eef3d052fe83dc688c0eb3dab97876d714a2d14d . 
.
docker image push renowx/realworldfront:vuejs
### 6.6.4
docker container run -ti --rm -p 8000:80 renowx/realworldfront:vuejs 
.
### 6.7.1
docker network create conduitnet
.
docker network ls
NETWORK ID          NAME                DRIVER              SCOPE
8ffed5eccb5a        bridge              bridge              local
0b289cb8bef6        conduitnet          bridge              local
5b106d05b9b8        host                host                local
819c7a68cf92        my_bridge           bridge              local
eeb693f5b705        none                null                local
.
### 6.7.2
docker volume ls
DRIVER              VOLUME NAME
local               c66fc03b1cc7be5447a458e33b5394b52255e551f6952f5e03de2c9833f4196d
local               dist
local               eacb15c2749d6aa23ff76cb4d8997427a69c33294f72dd587f2c8ca43ee5cfaa
local               gradle-home
local               nginx-logs
local               var-lib-mysql
.
### 6.7.3
docker container run --network conduitnet --name yncreadb -ti --rm -v var-lib-mysql:/var/lib/mysql mysql:5.7 --build-arg MYSQL_ROOT_PASSWORD=yncrea --build-arg MYSQL_DATABASE=conduit --build-arg MYSQL_USER=conduit --build-arg MYSQL_PASSWORD=superawesome 
.
